﻿Public Class Form1

    Public i As Integer
    'guardar los pesos en el tiempo
    Public peso1(i) As Decimal
    Public peso2(i) As Double
    Public peso3(i) As Double
    Public peso4(i) As Double
    Public prom As Double

    Dim prom1, prom2, prom3, prom4 As Double

    Public nom1, nom2, nom3, nom4 As String

    Private Sub cmdTerminar_Click(sender As Object, e As EventArgs) Handles cmdTerminar.Click
        Close()
    End Sub

    Private Sub txtPeso1Persona1_TextChanged(sender As Object, e As EventArgs) Handles txtPeso1Persona1.TextChanged

    End Sub

    Private Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click

        'Se guardan los pesos para luego agregarlos al arreglo

        Dim verify As Boolean


        'Verificar que las celdas de peso sean numeros y no textos
        If Not IsNumeric(txtPeso1Persona1.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True

        End If

        If Not IsNumeric(txtPeso2Persona1.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso3Persona1.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        'PERSONA 2
        If Not IsNumeric(txtPeso1Persona2.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso2Persona2.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso3Persona2.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        'PERSONA 3
        If Not IsNumeric(txtPeso1Persona3.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso2Persona3.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso3Persona3.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        'PERSONA 4
        If Not IsNumeric(txtPeso1Persona4.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso2Persona4.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If Not IsNumeric(txtPeso3Persona4.Text) Then
            MsgBox("Solo se aceptan numeros", 48)

        Else

            verify = True
        End If

        If verify Then

            Dim p1, p2, p3 As Decimal

            'Se guardan los valores de la persona 1
            p1 = Convert.ToDecimal(txtPeso1Persona1.Text)
            p2 = Convert.ToDecimal(txtPeso2Persona1.Text)
            p3 = Convert.ToDecimal(txtPeso3Persona1.Text)
            'Se guarda nombre de la persona 1
            nom1 = Convert.ToString(txtNombre1.Text)

            ReDim peso1(i)
            prom = (p1 + p2 + p3) / 3
            peso1.Append(prom)
            prom1 = prom


            txtPeso1Persona1.Text = ""
            txtPeso2Persona1.Text = ""
            txtPeso3Persona1.Text = ""

            'Se guardan los valores de la persona 2
            p1 = Convert.ToDecimal(txtPeso1Persona2.Text)
            p2 = Convert.ToDecimal(txtPeso2Persona2.Text)
            p3 = Convert.ToDecimal(txtPeso3Persona2.Text)
            'Se guarda nombre de la persona 2
            nom2 = Convert.ToString(txtNombre2.Text)

            ReDim peso1(i)
            prom = (p1 + p2 + p3) / 3
            peso2.Append(prom)
            prom2 = prom

            txtPeso1Persona2.Text = ""
            txtPeso2Persona2.Text = ""
            txtPeso3Persona2.Text = ""
            MsgBox("LA BASE DE DATOS HA SIDO ACTUALIZADA " + Convert.ToString(prom))

            'Se guardan los valores de la persona 3
            p1 = Convert.ToDecimal(txtPeso1Persona3.Text)
            p2 = Convert.ToDecimal(txtPeso2Persona3.Text)
            p3 = Convert.ToDecimal(txtPeso3Persona3.Text)
            'Se guarda nombre de la persona 3
            nom3 = Convert.ToString(txtNombre3.Text)

            ReDim peso2(i)
            prom = (p1 + p2 + p3) / 3
            peso3.Append(prom)
            prom3 = prom

            txtPeso1Persona3.Text = ""
            txtPeso2Persona3.Text = ""
            txtPeso3Persona3.Text = ""

            'Se guardan los valores de la persona 4
            p1 = Convert.ToDecimal(txtPeso1Persona4.Text)
            p2 = Convert.ToDecimal(txtPeso2Persona4.Text)
            p3 = Convert.ToDecimal(txtPeso3Persona4.Text)
            'Se guarda nombre de la persona 4
            nom4 = Convert.ToString(txtNombre4.Text)

            ReDim peso4(i)
            prom = (p1 + p2 + p3) / 3
            peso4.Append(prom)
            prom4 = prom
            txtPeso1Persona4.Text = ""
            txtPeso2Persona4.Text = ""
            txtPeso3Persona4.Text = ""

            i += 1
        End If


    End Sub

    Private Sub txtNombre1_TextChanged(sender As Object, e As EventArgs) Handles txtNombre1.TextChanged

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub txtPeso1Persona2_TextChanged(sender As Object, e As EventArgs) Handles txtPeso1Persona2.TextChanged

    End Sub

    Private Sub txtPeso1Persona3_TextChanged(sender As Object, e As EventArgs) Handles txtPeso1Persona3.TextChanged

    End Sub

    Private Sub txtPeso1Persona4_TextChanged(sender As Object, e As EventArgs) Handles txtPeso1Persona4.TextChanged

    End Sub

    Private Sub cmdHistorial_Click(sender As Object, e As EventArgs) Handles cmdHistorial.Click

        If i > 0 Then
            'mostrar en el Form2 la info y quien subio de peso
            Form2.txtHistorial.Text = nom1 & Space(10) + "subio de peso" & Space(10) + Convert.ToString(prom1)

            Form2.txthistorial1.Text = nom2 & Space(10) + "subio de peso" & Space(10) + Convert.ToString(prom2)

            Form2.Texthistorial2.Text = nom3 & Space(10) + "subio de peso" & Space(10) + Convert.ToString(prom3)

            Form2.Texthistorial3.Text = nom4 & Space(10) + "subio de peso" & Space(10) + Convert.ToString(prom4)

        End If



        Form2.Show()
        Me.Hide()
    End Sub
End Class
