﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.frmTitle = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdHistorial = New System.Windows.Forms.Button()
        Me.cmdTerminar = New System.Windows.Forms.Button()
        Me.txtNombre1 = New System.Windows.Forms.TextBox()
        Me.txtNombre2 = New System.Windows.Forms.TextBox()
        Me.txtNombre3 = New System.Windows.Forms.TextBox()
        Me.txtNombre4 = New System.Windows.Forms.TextBox()
        Me.txtPeso1Persona1 = New System.Windows.Forms.TextBox()
        Me.txtPeso2Persona1 = New System.Windows.Forms.TextBox()
        Me.txtPeso3Persona1 = New System.Windows.Forms.TextBox()
        Me.txtPeso3Persona2 = New System.Windows.Forms.TextBox()
        Me.txtPeso2Persona2 = New System.Windows.Forms.TextBox()
        Me.txtPeso1Persona2 = New System.Windows.Forms.TextBox()
        Me.txtPeso3Persona3 = New System.Windows.Forms.TextBox()
        Me.txtPeso2Persona3 = New System.Windows.Forms.TextBox()
        Me.txtPeso1Persona3 = New System.Windows.Forms.TextBox()
        Me.txtPeso3Persona4 = New System.Windows.Forms.TextBox()
        Me.txtPeso2Persona4 = New System.Windows.Forms.TextBox()
        Me.txtPeso1Persona4 = New System.Windows.Forms.TextBox()
        Me.cmdGuardar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'frmTitle
        '
        Me.frmTitle.AutoSize = True
        Me.frmTitle.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.frmTitle.Location = New System.Drawing.Point(323, 9)
        Me.frmTitle.Name = "frmTitle"
        Me.frmTitle.Size = New System.Drawing.Size(172, 25)
        Me.frmTitle.TabIndex = 0
        Me.frmTitle.Text = "¡Club Baja de Peso!"
        Me.frmTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label1.Location = New System.Drawing.Point(46, 65)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nombre"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label2.Location = New System.Drawing.Point(255, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 21)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Vascula 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label3.Location = New System.Drawing.Point(462, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 21)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Vascula 2"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.Label4.Location = New System.Drawing.Point(664, 65)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 21)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Vascula 3"
        '
        'cmdHistorial
        '
        Me.cmdHistorial.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdHistorial.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cmdHistorial.Location = New System.Drawing.Point(360, 396)
        Me.cmdHistorial.Name = "cmdHistorial"
        Me.cmdHistorial.Size = New System.Drawing.Size(96, 42)
        Me.cmdHistorial.TabIndex = 5
        Me.cmdHistorial.Text = "Historial"
        Me.cmdHistorial.UseVisualStyleBackColor = True
        '
        'cmdTerminar
        '
        Me.cmdTerminar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cmdTerminar.Location = New System.Drawing.Point(513, 396)
        Me.cmdTerminar.Name = "cmdTerminar"
        Me.cmdTerminar.Size = New System.Drawing.Size(92, 42)
        Me.cmdTerminar.TabIndex = 6
        Me.cmdTerminar.Text = "Terminar"
        Me.cmdTerminar.UseVisualStyleBackColor = True
        '
        'txtNombre1
        '
        Me.txtNombre1.Location = New System.Drawing.Point(46, 118)
        Me.txtNombre1.Name = "txtNombre1"
        Me.txtNombre1.Size = New System.Drawing.Size(175, 23)
        Me.txtNombre1.TabIndex = 7
        Me.txtNombre1.Text = "Nombre Apellido"
        '
        'txtNombre2
        '
        Me.txtNombre2.Location = New System.Drawing.Point(46, 184)
        Me.txtNombre2.Name = "txtNombre2"
        Me.txtNombre2.Size = New System.Drawing.Size(175, 23)
        Me.txtNombre2.TabIndex = 8
        Me.txtNombre2.Text = "Nombre Apellido"
        '
        'txtNombre3
        '
        Me.txtNombre3.Location = New System.Drawing.Point(46, 252)
        Me.txtNombre3.Name = "txtNombre3"
        Me.txtNombre3.Size = New System.Drawing.Size(175, 23)
        Me.txtNombre3.TabIndex = 9
        Me.txtNombre3.Text = "Nombre Apellido"
        '
        'txtNombre4
        '
        Me.txtNombre4.Location = New System.Drawing.Point(46, 327)
        Me.txtNombre4.Name = "txtNombre4"
        Me.txtNombre4.Size = New System.Drawing.Size(175, 23)
        Me.txtNombre4.TabIndex = 10
        Me.txtNombre4.Text = "Nombre Apellido"
        '
        'txtPeso1Persona1
        '
        Me.txtPeso1Persona1.Location = New System.Drawing.Point(255, 118)
        Me.txtPeso1Persona1.Name = "txtPeso1Persona1"
        Me.txtPeso1Persona1.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso1Persona1.TabIndex = 11
        '
        'txtPeso2Persona1
        '
        Me.txtPeso2Persona1.Location = New System.Drawing.Point(462, 118)
        Me.txtPeso2Persona1.Name = "txtPeso2Persona1"
        Me.txtPeso2Persona1.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso2Persona1.TabIndex = 12
        '
        'txtPeso3Persona1
        '
        Me.txtPeso3Persona1.Location = New System.Drawing.Point(664, 118)
        Me.txtPeso3Persona1.Name = "txtPeso3Persona1"
        Me.txtPeso3Persona1.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso3Persona1.TabIndex = 13
        '
        'txtPeso3Persona2
        '
        Me.txtPeso3Persona2.Location = New System.Drawing.Point(664, 184)
        Me.txtPeso3Persona2.Name = "txtPeso3Persona2"
        Me.txtPeso3Persona2.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso3Persona2.TabIndex = 16
        '
        'txtPeso2Persona2
        '
        Me.txtPeso2Persona2.Location = New System.Drawing.Point(462, 184)
        Me.txtPeso2Persona2.Name = "txtPeso2Persona2"
        Me.txtPeso2Persona2.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso2Persona2.TabIndex = 15
        '
        'txtPeso1Persona2
        '
        Me.txtPeso1Persona2.Location = New System.Drawing.Point(255, 184)
        Me.txtPeso1Persona2.Name = "txtPeso1Persona2"
        Me.txtPeso1Persona2.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso1Persona2.TabIndex = 14
        '
        'txtPeso3Persona3
        '
        Me.txtPeso3Persona3.Location = New System.Drawing.Point(664, 252)
        Me.txtPeso3Persona3.Name = "txtPeso3Persona3"
        Me.txtPeso3Persona3.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso3Persona3.TabIndex = 19
        '
        'txtPeso2Persona3
        '
        Me.txtPeso2Persona3.Location = New System.Drawing.Point(462, 252)
        Me.txtPeso2Persona3.Name = "txtPeso2Persona3"
        Me.txtPeso2Persona3.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso2Persona3.TabIndex = 18
        '
        'txtPeso1Persona3
        '
        Me.txtPeso1Persona3.Location = New System.Drawing.Point(255, 252)
        Me.txtPeso1Persona3.Name = "txtPeso1Persona3"
        Me.txtPeso1Persona3.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso1Persona3.TabIndex = 17
        '
        'txtPeso3Persona4
        '
        Me.txtPeso3Persona4.Location = New System.Drawing.Point(664, 327)
        Me.txtPeso3Persona4.Name = "txtPeso3Persona4"
        Me.txtPeso3Persona4.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso3Persona4.TabIndex = 22
        '
        'txtPeso2Persona4
        '
        Me.txtPeso2Persona4.Location = New System.Drawing.Point(462, 327)
        Me.txtPeso2Persona4.Name = "txtPeso2Persona4"
        Me.txtPeso2Persona4.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso2Persona4.TabIndex = 21
        '
        'txtPeso1Persona4
        '
        Me.txtPeso1Persona4.Location = New System.Drawing.Point(255, 327)
        Me.txtPeso1Persona4.Name = "txtPeso1Persona4"
        Me.txtPeso1Persona4.Size = New System.Drawing.Size(104, 23)
        Me.txtPeso1Persona4.TabIndex = 20
        '
        'cmdGuardar
        '
        Me.cmdGuardar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
        Me.cmdGuardar.Location = New System.Drawing.Point(211, 396)
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Size = New System.Drawing.Size(92, 42)
        Me.cmdGuardar.TabIndex = 23
        Me.cmdGuardar.Text = "Guardar"
        Me.cmdGuardar.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.cmdGuardar)
        Me.Controls.Add(Me.txtPeso3Persona4)
        Me.Controls.Add(Me.txtPeso2Persona4)
        Me.Controls.Add(Me.txtPeso1Persona4)
        Me.Controls.Add(Me.txtPeso3Persona3)
        Me.Controls.Add(Me.txtPeso2Persona3)
        Me.Controls.Add(Me.txtPeso1Persona3)
        Me.Controls.Add(Me.txtPeso3Persona2)
        Me.Controls.Add(Me.txtPeso2Persona2)
        Me.Controls.Add(Me.txtPeso1Persona2)
        Me.Controls.Add(Me.txtPeso3Persona1)
        Me.Controls.Add(Me.txtPeso2Persona1)
        Me.Controls.Add(Me.txtPeso1Persona1)
        Me.Controls.Add(Me.txtNombre4)
        Me.Controls.Add(Me.txtNombre3)
        Me.Controls.Add(Me.txtNombre2)
        Me.Controls.Add(Me.txtNombre1)
        Me.Controls.Add(Me.cmdTerminar)
        Me.Controls.Add(Me.cmdHistorial)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.frmTitle)
        Me.Name = "Form1"
        Me.Text = " ¡Club Baja de Peso!"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents frmTitle As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cmdHistorial As Button
    Friend WithEvents cmdTerminar As Button
    Friend WithEvents txtNombre1 As TextBox
    Friend WithEvents txtNombre2 As TextBox
    Friend WithEvents txtNombre3 As TextBox
    Friend WithEvents txtNombre4 As TextBox
    Friend WithEvents txtPeso1Persona1 As TextBox
    Friend WithEvents txtPeso2Persona1 As TextBox
    Friend WithEvents txtPeso3Persona1 As TextBox
    Friend WithEvents txtPeso3Persona2 As TextBox
    Friend WithEvents txtPeso2Persona2 As TextBox
    Friend WithEvents txtPeso1Persona2 As TextBox
    Friend WithEvents txtPeso3Persona3 As TextBox
    Friend WithEvents txtPeso2Persona3 As TextBox
    Friend WithEvents txtPeso1Persona3 As TextBox
    Friend WithEvents txtPeso3Persona4 As TextBox
    Friend WithEvents txtPeso2Persona4 As TextBox
    Friend WithEvents txtPeso1Persona4 As TextBox
    Friend WithEvents cmdGuardar As Button
End Class
